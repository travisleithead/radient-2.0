/* Simple Node.js file server (made with node.js 16)
/* "get started quick"
/* (c) July 2021 Travis Leithead
*/

const fs = require( 'fs' );
const http = require( 'http' );
const serveFrom = "./";

// See https://github.com/jshttp/mime-db/blob/master/db.json for more: add as needed
const mimeMap = {
  ".css": "text/css; charset=utf-8",
  ".htm": "text/html; charset=utf-8",
  ".html": "text/html; charset=utf-8",
  ".js": "text/javascript; charset=utf-8",
  ".json": "application/json",
  ".png": "image/png",
  ".ttf": "font/ttf"
};

(function start() {
  // Test that the 'serveFrom' location exists...
  let rootDir = null;
  if ( !serveFrom.endsWith( "/" ) ) {
    serveFrom += "/";
  }
  try {
    rootDir = fs.opendirSync( serveFrom );
  }
  catch (ex) {
    console.error( `* Config error. The value of '$serveFrom' (${serveFrom}) was not found.`);
    return 0; 
  }  
  let filesToServe = new Map(); // Map of filenames (including paths) to serve and their mimeType
  let filesToNotServe = new Map();
  let subFolderStack = [];
  let buildFilesSet = ( directory ) => {
    let dirEntry = null;
    while ( dirEntry = directory.readSync() ) {
      if ( dirEntry.isDirectory() ) {
        subFolderStack.push( dirEntry.name );
        buildFilesSet( fs.opendirSync( serveFrom + subFolderStack.join( '/' ) + "/" ) ); // just added a subFolderStack entry, so will always have at least one thing in the join call.
        subFolderStack.pop();
      }
      else if ( dirEntry.isFile() ) {
        // Only put files that the server knows how to map into the list.
        // What is the file extension?
        let extension = /\.[\S]+$/.exec( dirEntry.name )?.[0]; // Grab first result if non-null.
        // each entry is put into server-request format: "/path/filename.extension"
        let fullFileAndPath = `/${subFolderStack.join( '/' ) }${ subFolderStack.length > 0 ? "/" : "" }${ dirEntry.name }`;
        if ( extension && mimeMap[extension] ) {
          filesToServe.set( fullFileAndPath, mimeMap[extension] );
        }
        else {
          filesToNotServe.set( fullFileAndPath, extension ?? "<no extension detected>" );
        }
      }
    }
    directory.closeSync();
  };
  buildFilesSet( rootDir );
  console.log( `* Loaded ${ filesToServe.size } file(s) from '$serveFrom' directory (${ serveFrom }) including sub-folders. Ready.` );
  if ( filesToNotServe.size > 0 ) {
    console.log( `* Note: the following file(s) skipped due to unrecognized mimeType mapping. Please update '$mimeMap' if necessary: ` );
    for ( let [ key, value ] of filesToNotServe ) {
      console.log( `  | ${ key },  "${ value }"` );
    }
  }
  filesToNotServe = null; // Done with this Map.
  // Strip the end slash for combining with server file names later...
  let slashLessServeFrom = serveFrom.slice(0, -1);

  // Start server
  const server = http.createServer( (request, response) => {
    if ( request.method == "GET" && filesToServe.has( request.url ) ) {
      // Available file; return it.
      response.writeHead( 200, {"Content-Type": filesToServe.get( request.url ) } );
      response.write( fs.readFileSync( slashLessServeFrom + request.url ) );
      response.end();
      console.log( "-> " + request.url );
    }      
    else {
      response.writeHead( 404, {'Content-Type': 'text/html'} ); // not found
      response.write(`<!doctype html>
<style>
  @keyframes grow { from { opacity: 0.1; transform:translate(0, -40px) scale(0.95); } to { opacity: 1; transform:translate(0,0) scale(1.0); } }
  body { height:100vh; background-image:linear-gradient(to bottom, lightskyblue, lavender); font-family:Garamond; }
  h1 { font-size:42pt; text-align:center; animation: 1.5s ease 1 both grow; }
</style>
<body>
 <h1>Sorry. Requested '${ request.url }'</h1>
 <p>Simple file server</p>`);
      response.end();
      console.log( "? " + request.url );
    }
  } );
  
  server.listen( { port: 80 } );
  console.log( "Server started on port 80." );
  console.log( "  Serve files from: http://localhost/" );
})();