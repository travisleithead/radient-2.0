import { UtilityGun } from "./weapon.js";


const FORCE_STEP = 10;
const FORCE_STEP_PER_X_MS = 16; // 10 units per 16 ms (assuming 60 fps) or full force at 160 ms
const FORCE_MAX = 100;
const FORCE_MIN = -100;

function setForceNet( player, elapsedTime, keyState ) {
  // Note: This assumes linear growth and decay of force.
  let forceStep = Math.min( FORCE_MAX, FORCE_STEP * ( elapsedTime / FORCE_STEP_PER_X_MS ) );

  // Add impulse (force) in the left direction (negative force)
  if ( keyState.left ) {
    player.forceNet -= forceStep;
    if ( player.forceNet < FORCE_MIN ) {
      player.forceNet = FORCE_MIN;
    }
  }
  // Add impulse (force) in the right direction (positive force)
  else if ( keyState.right ) {
    player.forceNet += forceStep;
    if ( player.forceNet > FORCE_MAX ) {
      player.forceNet = FORCE_MAX;
    }
  }
  // Decay existing forces...(no impulse)
  else {
    if ( player.forceNet < 0 ) {
      player.forceNet += forceStep;
      if ( player.forceNet > 0 ) {
        player.forceNet = 0;
      }
    }
    else if ( player.forceNet > 0 ) {
      player.forceNet -= forceStep;
      if ( player.forceNet < 0 ) {
        player.forceNet = 0;
      }
    }
  }
}

const MOVE_MAX = 0.1; // 10% of the overal width (at full force)
const MOVE_MAX_PER_X_MS = 200; // 0.1 movement amount (max) each 200ms = 2000 ms to cross the entire width (or 200ms per 10%)

function setPos( player, elapsedTime ) {
  // Get the amount of movement per this time-interval (as a percent)
  let moveStepMaxPercentAtFullForce = Math.min( MOVE_MAX, MOVE_MAX * ( elapsedTime / MOVE_MAX_PER_X_MS ) );
  // Scale by the amount of force being currently applied...
  player.percentStep = moveStepMaxPercentAtFullForce * ( Math.abs( player.forceNet ) / FORCE_MAX );
  
  if ( player.forceNet > 0 ) {
    player.pos += player.percentStep;
    if ( player.pos > 1.0 ) {
      player.pos = 1.0;
    }
  }
  else if ( player.forceNet < 0 ) {
    player.pos -= player.percentStep;
    if ( player.pos < 0.0 ) {
      player.pos = 0;
    }
  }
}

const TRACKWIDTH_PERCENT = 0.96;
const TRACKMARGIN_PERCENT = ( 1.0 - TRACKWIDTH_PERCENT ) / 2;
const BOTTOM_MARGIN_PERCENT = 0.05; 

export default class Player {
  constructor() {
    this.forceNet = 0;    // Intermediate calculation for tracking player inertia
    this.percentStep = 0; // Final (percent-based) step amount to use to adjust 'pos'
    this.pos = 0.5;       // Where the player is right now (as a percent)
    this.weapon = new UtilityGun( 1, "green" );
  }
  drawPlayer( ctx, elapsedTime, keyState ) {
    // Given elapsed time and current keyboard actions...
    setForceNet( this, elapsedTime, keyState );
    setPos( this, elapsedTime );
  
    let canvasWidth = ctx.canvas.width;
    let canvasHeight = ctx.canvas.height;
  
    // Translate to actual numbers
    const trackWidth = canvasWidth * TRACKWIDTH_PERCENT;
    const margin = canvasWidth * TRACKMARGIN_PERCENT;
    const x = parseInt( margin + ( trackWidth * this.pos ) );
    const y = parseInt( canvasHeight - ( canvasHeight * BOTTOM_MARGIN_PERCENT ) );
  
    ctx.fillRect( x - 5, y - 5, 10, 10 );
  };
  shoot() {
    return this.weapon.fire( this.pos, 1, Math.PI / 2 );
  }
}