/* home-grown path animator */

// * mirrored X and Y (at clone time only)
// * get x,y at pos [0-1]
// to Path2D for rendering/testing.
const CONTROL_POINTS_RADIUS = 10;
const CONTROL_POINT_1_RADIUS = 8;
const CONTROL_POINT_2_RADIUS = 6;

export default class MotionPath2D {
  constructor( startX, startY ) {
    this.segmentsX = []; 
    this.segmentsY = [];
    this.startX = startX;
    this.startY = startY;
    this.boundsLeft = startX;
    this.boundsRight = startX;
    this.boundsTop = startY;
    this.boundsBottom = startY;
  }
  clone( mirror = null ) {
    let clone = new MotionPath2D( this.startX, this.startY );
    clone.boundsLeft = this.boundsLeft;
    clone.boundsRight = this.boundsRight;
    clone.boundsTop = this.boundsTop;
    clone.boundsBottom = this.boundsBottom;
    // mirroring flips all coordinates along the center
    // line of the bounding box. This is done by negating them
    // (mirroring around zero, then shifting the positions back 
    // into place)
    let horizontalAdjust = ( val ) => val; // Identity function
    let verticalAdjust = ( val ) => val;
    if ( mirror == "horizontal" ) {
      horizontalAdjust = ( val ) => ( -val + ( this.boundsRight - this.boundsLeft ) + ( 2 * this.boundsLeft ) );
    }
    else if ( mirror == "vertical" ) {
      verticalAdjust = ( val ) => ( -val + ( this.boundsBottom - this.boundsTop ) + ( 2 * this.boundsTop ) );
    }
    clone.startX = horizontalAdjust( this.startX );
    clone.startY = verticalAdjust( this.startY );
    clone.segmentsX = this.segmentsX.map( ( segmentArray ) => segmentArray.map( ( xCoord ) => horizontalAdjust( xCoord ) ) );
    clone.segmentsY = this.segmentsY.map( ( segmentArray ) => segmentArray.map( ( yCoord ) => verticalAdjust( yCoord ) ) );
    return clone;
  }
  // Build/extend the motion path
  lineTo( endX, endY ) {
    this.segmentsX.push( [ endX ] );
    this.segmentsY.push( [ endY ] );
    extendBounds( this, endX, endY );
  }
  quadradicCurveTo( controlX, controlY, endX, endY ) {
    this.segmentsX.push( [ controlX, endX ] );
    this.segmentsY.push( [ controlY, endY ] );
    extendBounds( this, controlX, controlY );
    extendBounds( this, endX, endY );
  }
  bezierCurveTo( controlX1, controlY1, controlX2, controlY2, endX, endY ) {
    this.segmentsX.push( [ controlX1, controlX2, endX ] );
    this.segmentsY.push( [ controlY1, controlY2, endY ] );
    extendBounds( this, controlX1, controlY1 );
    extendBounds( this, controlX2, controlY2 );
    extendBounds( this, endX, endY );
  }
  getBounds() {
    let path = new Path2D();
    path.rect( this.boundsLeft, this.boundsTop,
              ( this.boundsRight - this.boundsLeft ),
              ( this.boundsBottom - this.boundsTop ) );
    return path;
  }
  getPath2D() {
    let path = new Path2D();
    // include the control points for reference
    path.arc( this.startX, this.startY, CONTROL_POINTS_RADIUS, 0, Math.PI * 2 );
    for ( let i = 0, len = this.segmentsX.length; i < len; i++ ) {
      switch ( this.segmentsX[i].length ) {
        case 1: {
          path.moveTo( this.segmentsX[i][0] + CONTROL_POINTS_RADIUS, this.segmentsY[i][0] );
          path.arc( this.segmentsX[i][0], this.segmentsY[i][0], CONTROL_POINTS_RADIUS, 0, Math.PI * 2 ); break;
        }
        case 2: {
          path.rect( this.segmentsX[i][0] - CONTROL_POINTS_RADIUS, this.segmentsY[i][0] - CONTROL_POINTS_RADIUS,
                     CONTROL_POINTS_RADIUS * 2, CONTROL_POINTS_RADIUS * 2 );
          path.moveTo( this.segmentsX[i][1] + CONTROL_POINTS_RADIUS, this.segmentsY[i][1] );
          path.arc( this.segmentsX[i][1], this.segmentsY[i][1], CONTROL_POINTS_RADIUS, 0, Math.PI * 2 ); break;
        }
        case 3: {
          // double rect for the bezier 1st control point
          path.rect( this.segmentsX[i][0] - CONTROL_POINTS_RADIUS, this.segmentsY[i][0] - CONTROL_POINTS_RADIUS,
                     CONTROL_POINTS_RADIUS * 2, CONTROL_POINTS_RADIUS * 2 );
          path.rect( this.segmentsX[i][0] - CONTROL_POINT_1_RADIUS, this.segmentsY[i][0] - CONTROL_POINT_1_RADIUS,
                     CONTROL_POINT_1_RADIUS * 2, CONTROL_POINT_1_RADIUS * 2 );
          path.moveTo( this.segmentsX[i][1] + CONTROL_POINTS_RADIUS, this.segmentsY[i][1] );
          // triple rect for the bezier 2nd control point
          path.rect( this.segmentsX[i][1] - CONTROL_POINTS_RADIUS, this.segmentsY[i][1] - CONTROL_POINTS_RADIUS,
                     CONTROL_POINTS_RADIUS * 2, CONTROL_POINTS_RADIUS * 2 );
          path.rect( this.segmentsX[i][1] - CONTROL_POINT_1_RADIUS, this.segmentsY[i][1] - CONTROL_POINT_1_RADIUS,
                     CONTROL_POINT_1_RADIUS * 2, CONTROL_POINT_1_RADIUS * 2 );
          path.rect( this.segmentsX[i][1] - CONTROL_POINT_2_RADIUS, this.segmentsY[i][1] - CONTROL_POINT_2_RADIUS,
                     CONTROL_POINT_2_RADIUS * 2, CONTROL_POINT_2_RADIUS * 2 );
          path.moveTo( this.segmentsX[i][2] + CONTROL_POINTS_RADIUS, this.segmentsY[i][2] );
          path.arc( this.segmentsX[i][2], this.segmentsY[i][2], CONTROL_POINTS_RADIUS, 0, Math.PI * 2 ); break;
        }
      }
    }
    path.moveTo( this.startX, this.startY );
    for ( let i = 0, len = this.segmentsX.length; i < len; i++ ) {
      switch ( this.segmentsX[i].length ) {
        case 1: path.lineTo( this.segmentsX[i][0], this.segmentsY[i][0] ); break;
        case 2: path.quadraticCurveTo( this.segmentsX[i][0], this.segmentsY[i][0],
                                       this.segmentsX[i][1], this.segmentsY[i][1] ); break;
        case 3: path.bezierCurveTo( this.segmentsX[i][0], this.segmentsY[i][0],
                                    this.segmentsX[i][1], this.segmentsY[i][1],
                                    this.segmentsX[i][2], this.segmentsY[i][2] ); break;
      }
    }
    return path;
  }
  getPointAtTime( time = 0.0 ) { // between zero and 1.
    // Each path segment given equal distribution of time.
    let point = { x: this.startX, y: this.startY }; // default
    if ( this.segmentsX.length == 0 ) {
      return point;
    }
    // 1 or more segments
    // Which segment to choose?
    let segIndex = Math.floor( time * this.segmentsX.length ); // divide the segments list in equal parts, and use the time index to select the right one...
    segIndex = Math.max( Math.min( segIndex, this.segmentsX.length - 1 ), 0 ); // Ensure for time >= 1.0 OR < 0.0 that we get a good index.
    let segX = this.segmentsX[segIndex];
    let segY = this.segmentsY[segIndex];
    let lastX = this.startX;
    let lastY = this.startY;
    if ( segIndex > 0 ) {
      lastX = this.segmentsX[segIndex - 1].at(-1);
      lastY = this.segmentsY[segIndex - 1].at(-1);
    }
    // What's the relative time offset within this segment?
    let segmentTimeDistance = 1.0 / this.segmentsX.length; // Compute the time segment uniform "distance"
    let startOfTimeSegmentOffset = segmentTimeDistance * segIndex; // Find the point in time that starts the relative time for the chosen segment
    let relativeTimeOffset = ( time - startOfTimeSegmentOffset ) * this.segmentsX.length; // Get the offset vector relative to the start of the segment and scale it up to 100% of the segment size.
    // Define some handy bezier functions that use relativeTimeOffset
    let linearPoint = ( start, end ) => ( start + ( ( end - start ) * relativeTimeOffset ) );
    let quadPoint = ( start, mid, end ) => linearPoint( linearPoint( start, mid ), linearPoint( mid, end ) );
    let cubicPoint = ( start, part, most, end ) => quadPoint( linearPoint( start, part ), linearPoint( part, most ), linearPoint( most, end ) );
    switch ( segX.length ) {
      case 1: { // Linear interpolation
        point.x = linearPoint( lastX, segX[0] );
        point.y = linearPoint( lastY, segY[0] ); break;
      }
      case 2: { // Quadradic bezier interpolation
        point.x = quadPoint( lastX, segX[0], segX[1] );
        point.y = quadPoint( lastY, segY[0], segY[1] ); break;
      }
      case 3: { // Cubic bezier
        point.x = cubicPoint( lastX, segX[0], segX[1], segX[2] );
        point.y = cubicPoint( lastY, segY[0], segY[1], segY[2] ); break;
      }
    }
    return point; 
  }
};

function extendBounds( motionPath2D, x, y ) {
  motionPath2D.boundsLeft = Math.min( x, motionPath2D.boundsLeft );
  motionPath2D.boundsRight = Math.max( x, motionPath2D.boundsRight );
  motionPath2D.boundsBottom = Math.max( y, motionPath2D.boundsBottom );
  motionPath2D.boundsTop = Math.min( y, motionPath2D.boundsTop );
}