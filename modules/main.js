import getCanvas from "./canvas.js";
import player from "./player.js";
import { MyBullets } from "./bullet.js";
import getKeyState from "./keys.js";
import MotionPath2D from "./pathman.js";
import Formation from "./formationman.js";

const c = getCanvas( "maincanvas" );
const x = c.getContext( "2d" );
const k = getKeyState();

// start the render loop
var lastTime = 0;

let mp = new MotionPath2D(500, 100);
mp.lineTo( 600, 200 );
mp.quadradicCurveTo( 700, 300, 600, 150 );
mp.bezierCurveTo( 750, 200, 800, 100, 900, 200 );
let p = mp.getPath2D();

let squareFormation = new Formation(9, "n-packed-rows", 3 );

let vTime = 0.0;

const myBullets = new MyBullets();

function animationFrameFunction(currentTime) {
  let elapsedTime = currentTime - lastTime;
  lastTime = currentTime;
  x.beginPath();
  x.clearRect(0,0,c.width,c.height);
  
  x.fillStyle = "white";
  x.strokeStyle = "white";
  
  let bullet = player.shoot(); // every time, but the weapons have cooldown periods...
  if ( bullet ) {
    myBullets.bullets.push( bullet );
  }

  player.drawPlayer( x, elapsedTime, k );
  myBullets.draw( x, elapsedTime );
  
  x.stroke( p );
  x.fillStyle = "red";

  let { x: xpos, y: ypos } = mp.getPointAtTime( vTime );
  x.moveTo( xpos, ypos );
  x.arc( xpos, ypos, 4, 0, Math.PI*2);
  x.fill();
  vTime += 0.001;
  if ( vTime > 1 )
    vTime = 0;

  x.beginPath();
  x.rect( 50, 50, 200, 200 );
  x.stroke();
  x.beginPath();
  for ( let { x: xx, y } of squareFormation ) {
    x.rect( 50 + ( xx * 200 ), 50 + ( y * 200 ), 4, 4 );
  }
  x.fill();
  requestAnimationFrame( animationFrameFunction );
}

// Start the render loop
requestAnimationFrame( animationFrameFunction );