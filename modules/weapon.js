import Bullet from "./bullet.js";

class Weapon {
    constructor( type, level, damage, color ) {
        this.damage = Math.max( damage, 1 );
        this.color = color;
        this.coolingdown = null;
    }
    fire( x, y, direction ) { // in radians
        if ( !this.coolingdown ) {
            this.coolingdown = setTimeout( () => {
                // Cooldown is done! Ready for action!
                this.coolingdown = null;
            }, this.cooldown );
            let bullet = this.createBullet();
            bullet.color = this.color;
            bullet.damage = this.damage;
            bullet.x = x;
            bullet.y = y;
            bullet.direction = direction;
            return bullet;
        }
        return null;
    }
};

const UtilityGunBulletConfig = {
    velocity: 2,
    accelerationPerSecond: 0,
    range: 1000,
    boundLeft: -2,
    boundRight: 2,
    boundTop: -2,
    boundBottom: 2
};

export class UtilityGun extends Weapon {
    constructor( level, color ) {
        super( level /* equals damage */, color, 500 /* cooldown ms */ );
        this.level = level;
        this.cooldown = 500;
        
    }
    createBullet() {
        return new Bullet( UtilityGunBulletConfig );
    }
}
