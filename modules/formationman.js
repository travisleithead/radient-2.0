import MotionPath2D from "./pathman.js";

/* Given a formation index, return it's x/y coordinate */
/* Formation type (grid, packed, etc.) */
/* Grid size (number of things in the formation) */

export default class Formation {
  constructor( fleetSize = 1, name = "square", nameParam = 1 ) {
    this.indexPos = [];
    fleetSize = Math.max( fleetSize, 1 );
    if ( fleetSize == 1 ) { // Any formation of 1 is going to be in the center!
      this.indexPos.push( { x: 0.5, y: 0.5 } );
      return;
    }
    let placeFleetOnPaths = ( pathCollection, itemsPerPath, pack = false ) => {
      for ( let i = 0, remainingToPlace = fleetSize; remainingToPlace > 0; i++ ) {   
        let localLen = ( pack && ( i % 2 == 1 ) ? Math.max( itemsPerPath - 1, 1 ) : itemsPerPath );
        let addr = 1 / Math.max( localLen - 1, 1 );
        if ( remainingToPlace < localLen ) {
          localLen = remainingToPlace;
        }
        for ( let pos = 0, j = 0; j < localLen; pos += addr, j++ ) {
          this.indexPos.push( pathCollection[i].getPointAtTime( pos ) );
        }
        remainingToPlace -= localLen;
      }
    }
    if ( name == "n-rows" || name == "n-cols" ) {
      let lines = [];
      for ( let pos = 0, i = 0, addr = 1 / Math.max( nameParam - 1, 1 ); i < nameParam; pos += addr, i++ ) {
        let line = ( name == "n-rows" ? new MotionPath2D( 0, pos ) : new MotionPath2D( pos, 0 ) );
        line.lineTo( ( name == "n-rows" ? 1 : pos ), ( name == "n-rows" ? pos : 1 ) );
        lines.push( line );
      }
      placeFleetOnPaths( lines, Math.ceil( fleetSize / nameParam ) );
    }
    else if ( name == "n-packed-rows" || name == "n-packed-cols" ) {
      let packingLineLoss = Math.floor( nameParam / 2 );
      let itemsPerPath = Math.ceil( ( fleetSize + packingLineLoss ) / nameParam );
      let itemHalfGap = ( 1 / Math.max( itemsPerPath - 1, 1 ) ) / 2;
      let lines = [];
      for ( let pos = 0, i = 0, addr = 1 / Math.max( nameParam - 1, 1 ); i < nameParam; pos += addr, i++ ) {
        let line;
        if ( i % 2 == 0 ) {
          line = ( name == "n-packed-rows" ? new MotionPath2D( 0, pos ) : new MotionPath2D( pos, 0 ) );
          line.lineTo( ( name == "n-packed-rows" ? 1 : pos ), ( name == "n-packed-rows" ? pos : 1 ) );
        }
        else {
          line = ( name == "n-packed-rows" ? new MotionPath2D( itemHalfGap, pos ) : new MotionPath2D( pos, itemHalfGap ) );
          line.lineTo( ( name == "n-packed-rows" ? 1 - itemHalfGap : pos ), ( name == "n-packed-rows" ? pos : 1 - itemHalfGap ) );
        }
        lines.push( line );
      }
      placeFleetOnPaths( lines, itemsPerPath, true );
    }
    else if ( name == "square" ) {
      let square = new MotionPath2D( 0, 0 );
      square.lineTo( 1, 0 );
      square.lineTo( 1, 1 );
      square.lineTo( 0, 1 );
      square.lineTo( 0, 0 );
      let fleetSizeIncrement = 1 / fleetSize;
      for ( let fleetSizeCumulative = 0; fleetSizeCumulative < 1; fleetSizeCumulative += fleetSizeIncrement ) {
        this.indexPos.push( square.getPointAtTime( fleetSizeCumulative ) );
      }
    }
    else {
      throw new Error( `Formation name ${ name } is not a known formation.` );
    }
  }  
  *[Symbol.iterator]() { // make this object iterable
    for ( let i = 0, len = this.indexPos.length; i < len; i++ ) {
      yield this.indexPos[i];
    }
  }
}