
// ConfigOb = {
//   width (#)
//   height (#)
//   hp (hit points)
//   weapon (weapon type)
// }

export default class Enemy {
    constructor( configOb ) {
        this.pixelOffsetLeft = -Math.ceil( configOb.width / 2 );
        this.pixelOffsetRight = Math.ceil( configOb.width / 2 );
        this.pixelOffsetTop = -Math.ceil( configOb.height / 2 );
        this.pixelOffsetBottom = Math.ceil( configOb.height / 2 );
        this.hp = configOb.hp;
        this.weapon = configOb.weapon;
    }
};