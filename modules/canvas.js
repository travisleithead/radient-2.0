// dynamically update the set of fullscreen canvas contexts' dimensions when the computed
// style of the canvas changes.

var delayCommitResize = null;

const resizeHandler = () => {
  // Note, this event may fire frequently. Wait a spell (500ms)
  //  before commiting to a final value.
  if ( delayCommitResize != null ) {
    // Resize fired (again) while we are waiting...
    // Stop the current timer...
    clearTimeout( delayCommitResize ); 
  }

  // This is a first-event, or we need to reset the previously set timer.
  delayCommitResize = setTimeout( commitResize, 500);
};

window.addEventListener( 'resize', resizeHandler, { passive: true } );

const setOfFullScreenCanvases = new Set();

const commitResize = () => {
  delayCommitResize = null;
  for ( let canvas of setOfFullScreenCanvases ) {
    canvas.width = parseInt( getComputedStyle(canvas).width );
    canvas.height = parseInt( getComputedStyle(canvas).height );
  }
};

export default function setupFullScreenCanvas( canvasId ) {

  let canvas = document.querySelector( `#${ canvasId }` );
  setOfFullScreenCanvases.add( canvas );
  // ensure the size is set (eventually).
  resizeHandler();
  return canvas;
}