
// configOb {
//   velocity (pos #)
//   accelerationPerSecond (real #)
//   range (pos #)
//   x, y
//   direction
//   boundLeft
//   boundRight
//   boundTop
//   boundBottom
// }

export default class Bullet {
    constructor( configOb ) {
        this.initialVelocity = configOb.velocity;
        this.currentVelocity = this.initialVelocity;
        this.velocityChange = configOb.accelerationPerSecond;
        this.range = configOb.range;
        this.boundLeft = configOb.boundLeft;
        this.boundRight = configOb.boundRight;
        this.boundTop = configOb.boundTop;
        this.boundBottom = configOb.boundBottom;
        // Set outside of the constructor
        this.x = 0;
        this.y = 0;
        this.direction = 0;
        this.damage = 0;
        this.color = null;
    }
}

class BulletCollection {
    constructor() {
        this.bullets = [];
    }
    draw( ctx, elapsedTime ) {
        // Given elapsed time
        // Translate to actual numbers
        let canvasWidth = ctx.canvas.width;
        let canvasHeight = ctx.canvas.height;

        for ( let i = 0, len = this.bullets.length; i < len; i++ ) {
            let bullet = this.bullets[i];
            let absPosX = bullet.x * canvasWidth;
            let absPosY = bullet.y * canvasHeight;
            ctx.beginPath();
            ctx.moveTo( absPosX, absPosY );
            ctx.arc( absPosX, absPosX, 2, 0, Math.PI * 2 );
            ctx.fill();

            // Now move the bullet!
            // Math.cos( bullet.direction ) * bullet.velocity;

            // TODO
        }
    } 
}

export class MyBullets extends BulletCollection {
    constructor() {
        super();
    }
}

export class EnemyBullets extends BulletCollection {
    constructor() {
        super();
    }
}