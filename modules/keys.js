
var keyState = {
  left: false,
  right: false
};

function keyDownHandler( e ) {
  switch ( e.code ) {
    case 'ArrowLeft': keyState.left = true; break;
    case 'ArrowRight': keyState.right = true; break;
  }
}

document.addEventListener( 'keydown', keyDownHandler, { passive: true } );

function keyUpHandler( e ) {
  switch ( e.code ) {
    case 'ArrowLeft': keyState.left = false; break;
    case 'ArrowRight': keyState.right = false; break;
  }
}

document.addEventListener( 'keyup', keyUpHandler, { passive: true } );

export default function getKeyState() {
  return keyState;
}